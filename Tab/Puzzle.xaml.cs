﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Tab
{
    public partial class Puzzle : ContentPage
    {
        int count = 0;
        public Puzzle()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Puzzle)}:  ctor");
        }
        void Puzzle_clicked(object sender, System.EventArgs e)
        {
            
            int i = count % 2;
            if (i == 0)
            {
                PuzzleLabel.Text = "27";
            }
            else
                PuzzleLabel.Text = "";
            ++count;
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
