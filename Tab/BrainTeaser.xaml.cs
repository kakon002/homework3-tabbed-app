﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Tab
{
    public partial class BrainTeaser : ContentPage
    {
        int count = 0;
        public BrainTeaser()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(BrainTeaser)}:  ctor");
        }

        void Brain_Clicked(object sender, System.EventArgs e)
        {
            int i = count % 2;
            if (i == 0)
            {
                BrainLabel.Text = "They read the same when upside-down.";
            }
            else
                BrainLabel.Text = "";
            ++count;   
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
