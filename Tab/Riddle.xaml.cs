﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Tab
{
    public partial class Riddle : ContentPage
    {
        int count = 0;
        public Riddle()
        {
            InitializeComponent();
        }
        void Riddle_clicked(object sender, System.EventArgs e)
        {

            int i = count % 2;
            if (i == 0)
            {
                RiddleLabel.Text = "I am the letter E";
            }
            else
                RiddleLabel.Text = "";
            ++count;
        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
