﻿using System.Diagnostics;
using Xamarin.Forms;

namespace Tab
{
    public partial class TabPage : TabbedPage
    {
        public TabPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabPage)}:  ctor");
            InitializeComponent();

        }
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
